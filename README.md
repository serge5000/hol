1. forEach -> iterable - Consumer
2. iterable - Function - Collect ?
3. Predicate
4. BufferedReader - new functionality

Stream.

Collectors



A good example of this is the lack of efficient parallel operations over large collections of data. Java 8 allows you to write complex collection-processing algorithms, and simply by changing a single method call you can efficiently execute this code on multicore CPUs. In order to enable writing of these kinds of bulk data parallel libraries, however, Java needed a new language change: lambda expressions.

object-oriented programming is mostly about abstracting over data, while functional programming is mostly about abstracting over behavior. The real world has both of these things, and so do our programs, so we can and should learn from both influences.

The biggest language change in Java 8 is the introduction of lambda expressions—a compact way of passing around behavior.

A functional interface is an interface with a single abstract method that is used as the type of a lambda expression

A lambda expression is a method without a name that is used to pass around behavior as if it were data.

Lambdas are succinctly expressed single method classes that represent behavior.


Lambda Syntax: input arguments -> body

Java has numerous single method interfaces.

Pre-Java 8 Implementation -> Annonymus classes

Static Methods on Interfaces

A higher-order function is a function that either takes another function as an argument or returns a function as its result. It’s very easy to spot a higher-order function: just look at its signature.
